import os

try:
    import unittest2 as unittest
except ImportError:
    import unittest
import sourcemap
import json


class IntegrationTestCase(unittest.TestCase):
    def get_fixtures(self, base):
        if os.path.isfile('fixtures/%s.js' % base):
            source = open('fixtures/%s.js' % base).read()
        else:
            source = None

        minified = open('fixtures/%s.min.js' % base).read()
        min_map = open('fixtures/%s.min.map' % base).read()

        return source, minified, min_map

    def test_jquery(self):
        source, minified, min_map = self.get_fixtures('jquery')

        source_lines = source.splitlines()

        assert sourcemap.discover(minified) == 'jquery.min.map'

        index = sourcemap.loads(min_map)
        assert index.raw == json.loads(min_map)
        for token in index:
            # Ignore tokens that are None.
            # There's no simple way to verify they're correct
            if token.name is None:
                continue
            source_line = source_lines[token.src_line]
            start = token.src_col
            end = start + len(token.name)
            substring = source_line[start:end]

            # jQuery's sourcemap has a few tokens that are identified
            # incorrectly.
            # For example, they have a token for 'embed', and
            # it maps to '"embe', which is wrong. This only happened
            # for a few strings, so we ignore
            if substring[0] == '"':
                continue
            assert token.name == substring

    def test_coolstuff(self):
        source, minified, min_map = self.get_fixtures('coolstuff')

        source_lines = source.splitlines()

        assert sourcemap.discover(minified) == 'tests/fixtures/coolstuff.min.map'

        index = sourcemap.loads(min_map)
        assert index.raw == json.loads(min_map)
        for token in index:
            if token.name is None:
                continue

            source_line = source_lines[token.src_line]
            start = token.src_col
            end = start + len(token.name)
            substring = source_line[start:end]
            assert token.name == substring

    def test_unicode_names(self):
        _, _, min_map = self.get_fixtures('unicode')

        # This shouldn't blow up
        sourcemap.loads(min_map)

    def test_content_bundle(self):
        _, minified, min_map = self.get_fixtures('contentBundle')

        assert sourcemap.discover(minified) == 'tests/fixtures/contentBundle.min.map', sourcemap.discover(minified)

        index = sourcemap.loads(min_map)
        assert index.raw == json.loads(min_map)
        for token in index:
            if token.name is None:
                continue

            source_line = index.get_content_line(token.dst_line, token.dst_col)

            start = token.src_col
            end = start + len(token.name)
            substring = source_line[start:end]
            assert token.name == substring


    def test_content_bundle_context_lines(self):
        _, minified, min_map = self.get_fixtures('contentBundle')

        assert sourcemap.discover(minified) == 'tests/fixtures/contentBundle.min.map', sourcemap.discover(minified)

        index = sourcemap.loads(min_map)
        assert index.raw == json.loads(min_map)
        token = index.lookup(0, 305)

        source_lines = index.get_content_line(token.dst_line, token.dst_col,
                                                 num_context_lines=1)

        assert source_lines == """function foo() {\n    console.log(foobar)\n}"""
